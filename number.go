package helper

import (
	"errors"
	"math"
)

func Max(arr []int64) (result int64, err error) {

	result = math.MinInt64

	if len(arr) > 0 {
		result = arr[0]
		for _, i := range arr {
			if i > result {
				result = i
			}
		}
	} else {
		err = errors.New("Không tìm giá trị lớn nhất trong mảng rỗng ---- 222222")
	}
	return
}
